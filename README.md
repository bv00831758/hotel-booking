# Hotel Booking



## Getting started

In this hotel boking system I have used NodeJs for API building and MongoDb for data storage.

You can find 4 apis in this project were you can create booking, retrive booking, cancel booking and delete booking.

## Prerequisite

- [ ] [NodeJS installed in your system]
- [ ] [MongoDb installed in your system]
- [ ] [Postman installed in your system to test the apis]

## Project Configuration

Create .env file on the root directory and add the environment variables and there values. Please see the .env-example file for referance

## Installation

Install the npm packages/modules using following command.

```
npm install -g nodemon
npm install
```

## Run Project

To run this project use following command.

```
npm run dev
```
