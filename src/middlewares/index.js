const verifyBookingData = require('./verifyBookingData');
const verifyObject = require('./verifyObject');
module.exports = {
    verifyBookingData,
    verifyObject
}