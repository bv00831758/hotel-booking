const joi = require("joi");

const getFormatedDate = (dt) => {
    const cdt = new Date(dt);
    const month = cdt.getMonth() + 1;
    const day = cdt.getDate() + 1;
    const year = cdt.getFullYear();
    const checkInDate = new Date(month + '/' + day + '/' + year);
    return checkInDate;
}

const getDateDiff = (date1, date2) => {
	const diffDays = parseInt((date1 - date2) / (1000 * 60 * 60 * 24));
	return diffDays;
}

const validateCheckInDate = (value, helper) => {
    const currentDate = new Date();
    const checkInDate = getFormatedDate(value);
    const diffDays = getDateDiff(checkInDate, currentDate);
    // Check whether the checkin date is not from the past
    if (diffDays < 0) {
        return helper.message("CheckInDate must be todays date or future date.");
    }
    return value;
};


const validation = joi.object({
    firstName: joi.string().alphanum().min(3).max(25).trim(true).required(),
    lastName: joi.string().alphanum().min(3).max(25).trim(true).required(),
    email: joi.string().email().trim(true).required(),
    numOfGuest: joi.number().integer().min(1).max(3),
    checkInDate: joi.date().iso().required().custom(validateCheckInDate, 'custom validation checkin date'),
    checkOutDate: joi.date().iso().greater(joi.ref('checkInDate')).required()
}).custom((obj, helper) => {
    // validate checkout date
    const { checkInDate, checkOutDate } = obj;
    const checkInDt = getFormatedDate(checkInDate);
    const checkOutDt = new Date(checkOutDate);
    const checkoutDiffDays = getDateDiff(checkOutDt, checkInDt);
    if (checkoutDiffDays > 3) {
        return helper.message('You can not book a room for more than 3 days.');
    }
    return obj;
});

const checkBookingData = (req, res, next) => {
    const { body } = req;
    const payload = {
		firstName: body.firstName,
        lastName: body.lastName,
		email: body.email,
		numOfGuest: body.numOfGuest,
		checkInDate: body.checkInDate,
		checkOutDate: body.checkOutDate
	};

	const { error } = validation.validate(payload);

    if (error) {
        res.status(406).send({
            status: "FAILED",
            message: 'Error in Booking Data',
            errors: [error]
        });
        return;
	} else  {
        next();
    }
};

const veryfyBookingData = {
    checkBookingData
};

module.exports = veryfyBookingData;