const mongoose = require('mongoose');
isValidObjectId = (req, res, next) => {
    const objectId = req.params.id;
    if (mongoose.Types.ObjectId.isValid(objectId)) {
        next();
    } else {
        res.status(400).send({
            status: "FAILED",
            message: 'Invalid id',
            errors: ['Id is not valid.']
        });
        return;
    }
};

const verifyObject = {
    isValidObjectId
};
module.exports = verifyObject;