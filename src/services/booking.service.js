const db = require('../models');
const BookingModel = db.booking;

const getBookingById = async (bookingId) => {
    const response = {
        isError: false,
        message: '',
        data: ''
    };
    try{
        const bookingDetails = await BookingModel.findById(bookingId);
        if (bookingDetails) {
            response.data = bookingDetails;
        } else {
            response.isError = true;
            response.message = 'Booking not found.';
            response.data = response.message;
        }
        return response;
    } catch (e) {
        response.isError = true;
        response.message = 'Something went wrong.';
        response.data = e;
        return response;
    }
};

const createNewBooking = async (newBooking) => {
    const response = {
        isError: false,
        message: '',
        data: ''
    };
    const bookingToInsert = {
        ...newBooking,
        createdAt: new Date().toLocaleString("en-US", { timeZone: "UTC" }),
        updatedAt: new Date().toLocaleString("en-US", { timeZone: "UTC" }),
    };
    try {
        const booking = new BookingModel(bookingToInsert);
        const result = await booking.save();
        response.data = result.toObject();
        return response;
    } catch (e) {
        response.isError = true;
        response.message = 'Something went wrong.';
        response.data = e;
        return response;
    }
};

const updateBooking = async (id, data) => {
    const response = {
        isError: false,
        message: '',
        data: ''
    };
    try{
        const updatedResult = await BookingModel.findByIdAndUpdate(id, data, {new: true});
        response.data = updatedResult;
        return response;
    } catch (e) {
        response.isError = true;
        response.message = 'Something went wrong.';
        response.data = e;
        return response;
    }
};

const deleteBooking = async (id) => {
    const response = {
        isError: false,
        message: '',
        data: ''
    };

    try{
        const deletedResult = await BookingModel.findByIdAndDelete(id);
        if (deletedResult) {
            response.data = deletedResult;
        } else {
            response.isError = true;
            response.message = 'Booking not found.';
            response.data = response.message;
        }
        return response;
    } catch (e) {
        response.isError = true;
        response.message = 'Something went wrong.';
        response.data = e;
        return response;
    }
};

module.exports = {
    getBookingById,
    createNewBooking,
    updateBooking,
    deleteBooking,
};