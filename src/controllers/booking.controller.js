const { response } = require("express");
const bookingService = require("../services/booking.service");

const getBookingById = async (req, res) => {
    const bookingId = req.params.id;
    const bookingDetails = await bookingService.getBookingById(bookingId);
    if (!bookingDetails.isError) {
        res.status(201).send({ status: "OK", data: bookingDetails.data });
    } else {
        res.status(404).send({ status: "FAILED", message: bookingDetails.message, errors: [bookingDetails.data] });
    }
};

const createNewBooking = async (req, res) => {
    const { body } = req;
    const newBooking = {
        email: body.email,
        firstName: body.firstName,
        lastName: body.lastName,
        numOfGuest: body.numOfGuest,
        checkInDate: body.checkInDate,
        checkOutDate: body.checkOutDate
      };
    const createdBooking = await bookingService.createNewBooking(newBooking);
    if (!createdBooking.isError) {
        res.status(201).send({ status: "OK", data: createdBooking.data });
    } else {
        res.status(400).send({ status: "FAILED", message: response.message, errors: [response.data] });
    }
    
};

const cancellBooking = async (req, res) => {
    const bookingId = req.params.id;
    const bookingDetails = await bookingService.getBookingById(bookingId);
    if (!bookingDetails.isError) {
        const dataToUpdate = {
            isCancelled: true,
            updatedAt: new Date().toLocaleString("en-US", { timeZone: "UTC" })
        };
        const updatedBooking = await bookingService.updateBooking(bookingId, dataToUpdate);
        if (!updatedBooking.isError) {
            res.status(201).send({ status: "OK", data: updatedBooking.data });
        } else {
            res.status(500).send({ status: "FAILED", message: updatedBooking.message, errors: [updatedBooking.data] });
        }
    } else {
        res.status(404).send({ status: "FAILED", message: bookingDetails.message, errors: [bookingDetails.data] });
    }
};

const deleteBooking = async (req, res) => {
    const bookingId = req.params.id;
    const deletedBooking = await bookingService.deleteBooking(bookingId);
    if (!deletedBooking.isError) {
        res.status(201).send({ status: "OK", data: deletedBooking.data });
    } else {
        res.status(500).send({ status: "FAILED", message: deletedBooking.message, errors: [deletedBooking.data] });
    }
};

module.exports = {
    getBookingById,
    createNewBooking,
    cancellBooking,
    deleteBooking,
};