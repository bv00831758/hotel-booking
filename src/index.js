const express = require("express");
const bodyParser = require("body-parser");
const v1BookingRouter = require("./v1/routes/booking.route");
const db = require("./models");
const {PORT, DB_HOST, DB_PORT, DB_NAME} = require("./config/config");

const app = express();
app.use(bodyParser.json());

// Booking Routes
app.use("/api/v1/booking", v1BookingRouter);

// Connect to database
db.mongoose
  .connect(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

app.listen(PORT, () => { 
    console.log(`API is listening on port ${PORT}`); 
});