const express = require("express");
const { verifyBookingData, verifyObject } = require("../../middlewares");
const bookingController = require("../../controllers/booking.controller");

const router = express.Router();

router.get("/:id", [verifyObject.isValidObjectId], bookingController.getBookingById);

router.post("/", [verifyBookingData.checkBookingData], bookingController.createNewBooking);

router.patch("/cancel/:id", [verifyObject.isValidObjectId], bookingController.cancellBooking);

router.delete("/:id", [verifyObject.isValidObjectId], bookingController.deleteBooking);

module.exports = router;