const dotenv = require('dotenv');

// Read the .env file and collects the variables in the envs object
const result = dotenv.config();
if (result.error) {
  throw result.error;
}
const { parsed: envs } = result;
module.exports = envs;