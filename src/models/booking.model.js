const mongoose=require('mongoose');
 
const BookingSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true
    },
    numOfGuest: {
        type: Number,
        required: true,
        trim: true
    },
    checkInDate: {
        type: Date,
        required: true,
        trim: true
    },
    checkOutDate: {
        type: Date,
        required: true,
        trim: true
    },
    isCancelled: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: Date
    },
    updatedAt: {
        type: Date
    }
});
 
module.exports = mongoose.model('booking', BookingSchema);